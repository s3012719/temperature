package nl.utwente.di.temperature;

import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * Example of a Servlet that gets the degrees in Celsius and returns the degrees in Fahrenheit
 */

public class Temperature extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Degrees in Fahrenheit";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project

        try {
            double fahrenheit = converter.convert(Double.parseDouble(request.getParameter("degrees")));
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Degrees in Celsius: " +
                    request.getParameter("degrees") + "\n" +
                    "  <P>Degrees in Fahrenheit: " + fahrenheit +
                    "</BODY></HTML>");
        } catch (NumberFormatException e) {
            System.out.println("User did not input a double");
            out.println("<!DOCTYPE HTML>\n" +
                    "<HTML>\n" +
                    "<HEAD><TITLE>" + title + "</TITLE>" +
                    "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                    "</HEAD>\n" +
                    "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                    "<H1>" + title + "</H1>\n" +
                    "  <P>Put in a valid number!" +
                    "</BODY></HTML>");
        }

    }
}

