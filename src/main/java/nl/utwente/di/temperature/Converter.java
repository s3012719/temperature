package nl.utwente.di.temperature;

public class Converter {
    public double convert(double celcius) {
        return (celcius * 9/5) + 32;
    }
}
