package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testBook1() throws Exception {
        Converter converter = new Converter();

        Assertions.assertEquals(32.0, converter.convert(0));

        Assertions.assertEquals(53.6, converter.convert(12));

        Assertions.assertEquals(-4, converter.convert(-20));
    }
}
